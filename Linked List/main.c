//
//  main.c
//  Linked List
//
//  Created by Jason Edstrom on 3/25/14.
//  Copyright (c) 2014 Jason Edstrom. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

const char print[7] = "print";
const char done[7] = "exit";
const char insert[7] = "insert";


typedef struct node{
    int data;
    struct node *ptr;
} node;

/*
 *  adding Node at the proper point in the stack and returns the head.
 *  parameter: head - head node of stack
 *  parameter: num - data for node
 *
 */
node* addNode(node* head, int num);


/*
 *  handles number input and passes to addNode method and returns the head
 *  parameter: head - head node of stack
 *  parameter: num - data for node
 *
 */
node* addToStack(node *head, int num);

/*
 *  Traverses the stack and prints the numbers
 *  parameter: head - head node of stack
 *
 */
void printStack(node *head);

/*
 *  Traverse the stack and frees the memory before exiting the program.
 *  parameter: head - head node of stack
 *
 */
void free_list(node *head);


node* addNode(node* head, int num) {
    node *temp, *prev, *next;
    temp = (node*)malloc(sizeof(node));
    temp->data = num;
    temp->ptr = NULL;
    if(!head){
        head=temp;
    } else{
        prev = NULL;
        next = head;
        while(next && next->data<=num){
            prev = next;
            next = next->ptr;
        }
        if(!next){
            prev->ptr = temp;
        } else{
            if(prev) {
                temp->ptr = prev->ptr;
                prev-> ptr = temp;
            } else {
                temp->ptr = head;
                head = temp;
            }
        }
    }
    return head;
}

node* addToStack(node *head, int num){
    printf("Enter a number: ");
        scanf("%d",&num);
          if(num) {
           head = addNode(head, num);
           }
    return head;
}

void printStack(node *head){
    node *p;
    p = head;
    printf("The numbers are: ");
    while(p) {
        printf("%d ", p->data);
        p = p->ptr;
    }
    printf("\n");
}


void free_list(node *head) {
    node *prev = head;
    node *cur = head;
    while(cur) {
        prev = cur;
        cur = prev->ptr;
        free(prev);
    }
}

int main(int argc, const char * argv[])
{
    char input[7];
    int num = 0;
    node *head;
    head = NULL;
    do {
        printf("Command:");
        scanf("%s", input);
        if (strcmp(input, insert) == 0){
            head = addToStack(head, num);
        } else if (strcmp(input, print) == 0){
            printStack(head);
        } else if (strcmp(input, done) == 0){
            break;
        } else {
            printf("Error: unknown request - %s\n", input);
        }
    } while (strcmp(input, done) != 0);
        free_list(head);
    printf("Exiting...");
    return 0;

    
}

